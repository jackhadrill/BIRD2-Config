FROM ubuntu:20.04
EXPOSE 179/tcp

# BIRD really doesn't like to be installed in a minimal 
# Ubuntu container, so some fudges are needed to get it to run.
RUN echo "path-include=/usr/share/doc/bird2/*" > /etc/dpkg/dpkg.cfg.d/include-bird
RUN mkdir -p /run/bird

RUN apt-get update && apt-get install -y bird2 && rm -rf /var/lib/apt/lists/*

# Useful debug packages. Not essential for BIRD to work.
RUN apt-get update && apt-get install -y iproute2 iputils-ping && rm -rf /var/lib/apt/lists/*

CMD ["/demo/start.sh"]