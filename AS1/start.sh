#!/bin/bash

# This adds the IPs AS1 announces such that they're
# pingable from AS2 once the routes have propagated.
ip address add 10.0.0.1/32 dev lo
ip address add 10.0.0.2/32 dev lo
ip address add 10.0.0.3/32 dev lo
ip -6 address add fd00:dead:beef::1/128 dev lo
ip -6 address add fd00:dead:beef::2/128 dev lo
ip -6 address add fd00:dead:beef::3/128 dev lo

# Start bird.
bird -d -c /demo/bird.conf