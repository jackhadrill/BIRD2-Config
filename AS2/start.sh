#!/bin/bash

# Add the IPs AS2 announces such that they're 
# pingable from AS1 once the routes have propagated.
ip address add 172.16.1/32 dev lo
ip address add 172.16.2/32 dev lo
ip address add 172.16.3/32 dev lo
ip -6 address add fd00:cafe:babe::1/128 dev lo
ip -6 address add fd00:cafe:babe::2/128 dev lo
ip -6 address add fd00:cafe:babe::3/128 dev lo

# Start bird.
bird -d -c /demo/bird.conf