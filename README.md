## Notes

BGP operates on port TCP 179. 
This is exposed to host.
- AS1 on 17901 
- AS2 on 17902

BGP requires peers to be on the same L2 network such that routes between routers can be established.
However, a lot of route collectors (people who just hoover up information about the state of the internet),
internet exchanges and ISPs expose TCP 179 publicly. Interaction with BGP can occur across the internet if
this port is left unfirewalled, but the routes it establishes will only ever be functional across a L2 network.

The configuration for BIRD 1.6 differs slightly. The main difference is that IPv4 and IPv6 operate as two 
separate processes and as such are configured using separate configuration files.

To start:
```bash
$ docker-compose up -d
```

To destroy:
```bash
$ docker-compose down --rmi all
```

To interact:
```bash
$ docker ps
CONTAINER ID  IMAGE          COMMAND           CREATED         STATUS         PORTS                   NAMES
8e6ec23b0482  bgpdemo/bird   "/demo/start.sh"  30 minutes ago  Up 30 minutes  0.0.0.0:17901->179/tcp  AS1
636ea235359c  bgpdemo/bird   "/demo/start.sh"  30 minutes ago  Up 30 minutes  0.0.0.0:17902->179/tcp  AS2

$ docker exec -it AS1 /bin/bash

$$ ip route show
default via 192.168.179.254 dev eth0 
172.16.0.1 via 192.168.179.2 dev eth0 proto bird src 192.168.179.1 metric 32 
172.16.0.2 via 192.168.179.2 dev eth0 proto bird src 192.168.179.1 metric 32 
172.16.0.3 via 192.168.179.2 dev eth0 proto bird src 192.168.179.1 metric 32 
192.168.179.0/24 dev eth0 proto kernel scope link src 192.168.179.1

$$ ping 172.16.0.1
PING 172.16.0.1 (172.16.0.1) 56(84) bytes of data.
64 bytes from 172.16.0.1: icmp_seq=1 ttl=64 time=0.081 ms
64 bytes from 172.16.0.1: icmp_seq=2 ttl=64 time=0.150 ms
64 bytes from 172.16.0.1: icmp_seq=3 ttl=64 time=0.167 ms
^C
--- 172.16.0.1 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2061ms
rtt min/avg/max/mdev = 0.081/0.132/0.167/0.037 ms

$$ birdc show protocol all
BIRD 2.0.7 ready.
Name       Proto      Table      State  Since         Info
kernel1    Kernel     master4    up     22:58:19.431  
  Channel ipv4
    State:          UP
    Table:          master4
    Preference:     10
    Input filter:   ACCEPT
    Output filter:  (unnamed)
    Routes:         0 imported, 3 exported, 0 preferred
    Route change stats:     received   rejected   filtered    ignored   accepted
      Import updates:              0          0          0          0          0
      Import withdraws:            0          0        ---          0          0
      Export updates:              9          0          6        ---          3
      Export withdraws:            0        ---        ---        ---          0
...
```

## Configuration 

### AS1

BGP port exposed on TCP 17901
ASN: `AS1`  
IPv4: `192.168.179.1`  
IPv6: `fd00::1`  
Announced routes:
```
10.0.0.1/32
10.0.0.2/32
10.0.0.3/32
fd00:dead:beef::1/128
fd00:dead:beef::2/128
fd00:dead:beef::3/128
```

### AS2

BGP port exposed on TCP 17902
ASN: `AS2`  
IPv4: `192.168.179.2`  
IPv6: `fd00::2`  
Announced routes:
```
172.16.0.1/32
172.16.0.2/32
172.16.0.3/32
fd00:cafe:babe::1/128
fd00:cafe:babe::2/128
fd00:cafe:babe::3/128
```
